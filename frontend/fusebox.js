const { FuseBox, WebIndexPlugin, CSSPlugin } = require('fuse-box');
const fuse = FuseBox.init({
  sourceMaps: true,
  homeDir: 'src',
  target: 'browser@es6',
  output: 'dist/$name.js',
  plugins: [
    WebIndexPlugin({
      template: 'src/assets/index.html',
    }),
    CSSPlugin(),
  ],
});
fuse.dev();
fuse
  .bundle('app')
  .instructions(' > app/index.ts')
  .hmr()
  .watch();
fuse.run();
