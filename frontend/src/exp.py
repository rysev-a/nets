from PIL import Image

def get_bin_from_image(image_path):
    image = Image.open(image_path)
    width, height = image.size
    loaded_image = image.load()

    res = []
    for i in range(0, 1536)[::8]:
        if loaded_image[2 + i,2][1] > 200:
            res.append('0')
        else:
            res.append('1')

    res = ''.join(res)

    return res
