import { makeDOMDriver, h2, div } from '@cycle/dom';
import { of, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { run } from '@cycle/rxjs-run';
import { ethernet } from './ethernet';

import 'bulma/css/bulma.min.css';
import signals from './signals';

const main = sources => {
  const signalsObj = signals(sources);

  const vdom$ = combineLatest(signalsObj.DOM).pipe(
    map(signalsView =>
      div(
        '.container',
        div('.columns', [
          div('.column', [h2('.title', 'nets'), ethernet.view()]),
          div('.column', signalsView),
        ])
      )
    )
  );

  const sinks = {
    DOM: vdom$,
  };

  return sinks;
};

const drivers = {
  DOM: makeDOMDriver('#app'),
};

run(main, drivers);
