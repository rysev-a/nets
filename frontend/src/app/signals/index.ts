import { div, h2, button } from '@cycle/dom';
import { map, flatMap, startWith, scan, mapTo } from 'rxjs/operators';
import { of } from 'rxjs';
import { style } from 'typestyle';

const signalButtonCss = style({
  cursor: 'pointer',
});

export const Signals = sources => {
  const domSource = sources.DOM;
  const props$ = sources.props;

  const newValue$ = domSource
    .select('.signals')
    .events('click')
    .pipe(
      mapTo(5),
      scan((acc, val) => acc + val, 0)
    );

  newValue$.subscribe(val => console.log(val));

  const state$ = props$.pipe(
    map((props: any) =>
      newValue$.pipe(
        map((val: any) => val),
        startWith(props.value)
      )
    ),
    flatMap((data: any) => data)
  );

  const vdom$ = state$.pipe(
    map((value: any) =>
      div([
        h2('.title', 'signals'),
        h2(`Signals value: ${value}`),
        button(`.signals ${signalButtonCss}`, 'click'),
      ])
    )
  );

  const sinks = {
    DOM: vdom$,
    value: state$.pipe(map(state => state)),
  };

  return sinks;
};

const signals = sources => {
  const signalsSources = {
    props: of({ value: 9000 }),
    DOM: sources.DOM,
  };

  return Signals(signalsSources);
};

export default signals;
