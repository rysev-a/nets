import * as R from 'ramda';
import decodeMap from './decode-map';

const splitToBlocks = data =>
  R.reduce((acc: number[][], item: number) => {
    if (acc.length && acc[acc.length - 1].length < 4) {
      acc[acc.length - 1].push(item);
    } else {
      const newBlock = [];
      newBlock.push(item);
      acc.push(newBlock);
    }

    return acc;
  }, [])(data);

const convertBlockToHex = block =>
  parseInt(block.join(''), 2)
    .toString(16)
    .toUpperCase();

const groupByTwoSymbols = data =>
  R.reduce((acc: string[][], el: string) => {
    if (acc.length && acc[acc.length - 1].length < 2) {
      acc[acc.length - 1].push(el);
    } else {
      const newBlock = [];
      newBlock.push(el);
      acc.push(newBlock);
    }

    return acc;
  }, [])(data);

const joinGroups = R.map((el: string[]) => el.join(''));

const splitToSymbols = R.reduce(
  (acc, item: string) => [...acc, parseInt(item)],
  []
);

export const convertBinToAscii = R.compose(
  R.reduce((acc, el) => acc + el, ''),
  R.map((el: string) => decodeMap[el]),
  joinGroups,
  groupByTwoSymbols,
  R.map(convertBlockToHex),
  splitToBlocks,
  splitToSymbols
);
