const decodeMap = {};
const win1251 = new TextDecoder('windows-1251');

for (let i = 0x00; i < 0xff; i++) {
  const hex = (i <= 0x0f ? '0' : '') + i.toString(16).toUpperCase();
  decodeMap[hex] = win1251.decode(Uint8Array.from([i]));
}

export default decodeMap;
