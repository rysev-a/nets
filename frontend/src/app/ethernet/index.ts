import * as R from 'ramda';
import imageData from './image-data';
import { convertBinToAscii } from './helpers';
import { div, h2, ul, li } from '@cycle/dom';

export const ethernet = {
  view: () => {
    const ethernetView = div('.ethernet-view', [
      h2('ethernet view'),
      ul(
        R.map((key: string) =>
          li(`${key} - ${convertBinToAscii(imageData[key])}`)
        )(Object.keys(imageData))
      ),
    ]);

    return ethernetView;
  },
};
