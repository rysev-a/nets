### Nets - learn computer networks

## Backend

Backend main packages:

- flask
- flask_restful
- sqlalchemy

## Frontend

Frontend main packages:

- cyclejs
- rxjs
- ramda

