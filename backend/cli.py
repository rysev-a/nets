from flask_kit import create_app, db
import os
import click
from app import create_app

"""
How to:
1. `export FLASK_APP=cli.py`
2. `flask run`
"""
app = create_app(os.environ['SETTINGS_ENV'] or 'app.settings.development')
