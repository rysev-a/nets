from flask_kit import api
from .resources import SignalDetail, SignalList


def init(app):
    api.add_resource(SignalList, '/api/v1/signals')
    api.add_resource(SignalDetail, '/api/v1/signals/<int:id>')
