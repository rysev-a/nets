from flask_kit import db


class Signal(db.Model):
    __tablename__ = 'signals'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(length=200), unique=True)
