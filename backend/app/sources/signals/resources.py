from flask_restful import Resource
from flask_kit import db
from flask_restful_crud import ListResource, DetailResource
from .models import Signal
from .fields import signal_fields


class SignalList(ListResource):
    model = Signal
    fields = signal_fields
    db = db


class SignalDetail(DetailResource):
    model = Signal
    fields = signal_fields
    db = db


class SignalUpload(Resource):
    def post(self):
        return {
            'message': 'upload complete'
        }
