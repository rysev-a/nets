from flask_restful import fields

signal_fields = {
    'id': fields.Integer,
    'title': fields.String
}
