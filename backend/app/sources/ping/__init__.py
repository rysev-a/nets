from flask_kit import api
from flask_restful import Resource


class Ping(Resource):
    def get(self):
        return {'message': 'pong'}


def init(app):
    api.add_resource(Ping, '/api/v1/ping')
